import React from 'react'
import axios from 'axios'
import './JokeList.css'
import emojirofl from '../assets/icons/emojiRofl.svg'
import {v4 as uuid} from 'uuid'
import Joke from './Joke'
import MyLoaders from '../assets/loaders/MyLoaders'

export class JokeList extends React.Component {
	static defaultProps = {
		noOfJokesToGet: 10
	}

	constructor(props) {
		super(props);
		this.state = { rerendering: false, loading:false, jokes: JSON.parse(window.localStorage.getItem("jokes") || "[]") }
		this.getJokes = this.getJokes.bind(this);
		this.handleVotes = this.handleVotes.bind(this);
		this.seenJokes = new Set(this.state.jokes.map(jk => jk.thejoke))
	}

	async getJokes() {
		try{
			console.log('getJokes invoked');
			this.setState({ loading:true })
			let jokes = [];
			while(jokes.length < this.props.noOfJokesToGet) {
				/*let res = await axios.get('https://icanhazdadjoke.com/', { headers: { Accept: 'application/json' } });*/
				let res = await axios.get('https://hindi-jokes-api.onrender.com/jokes?api_key=a69ee0e09404beb0ba19c0dec93f');
				if (!this.seenJokes.has(res.data.jokeContent))
				jokes.push({ id: uuid(), thejoke: res.data.jokeContent, votes:0 })
			}
			/*this.setState({ jokes: [...this.state.jokes, ...jokes] })*/
			this.setState(
				curState => ({ jokes: [...curState.jokes, ...jokes], loading: false }), 
				() => window.localStorage.setItem('jokes', JSON.stringify(this.state.jokes))
			);
		}catch(err){
			console.log(err); 
			this.setState({ loading:false })
		};
	}

	componentDidMount() {
		console.log('componentDidMount invoked');
		if (this.state.jokes.length === 0) {this.getJokes()}
	}

	componentDidUpdate() {
		console.log('rerender');
	}

	handleVotes(jokeid, delta) {
		let revisedJokes, sortedJokes = [];
		revisedJokes = this.state.jokes.map(jk => jk.id === jokeid ? { ...jk, votes: jk.votes+delta } : jk)
		sortedJokes = [...revisedJokes].sort((a,b) => b.votes - a.votes);
		this.setState(curState => ({ jokes: [...sortedJokes] }), () => window.localStorage.setItem('jokes', JSON.stringify(this.state.jokes)))
		// this.setState(curState => ({ jokes: curState.jokes.map(jk => jk.id === jokeid ? { ...jk, votes: jk.votes+delta }: jk) }));
		// setTimeout(()=> { this.setState({ rerendering: true }) }, 1000);
	}

	render() {
		const blurStyle = this.state.rerendering ? { backgroundColor: 'purple' } : {};
		return (
			<>
				{this.state.loading?
				(<div className = 'jokelist'>
					<div className = 'jokelist-sidebar'>
						<h1 className = 'jokelist-title'><span>Dad</span> Jokes</h1>
						<img src = {emojirofl} alt = 'कुछ कर भाई'/>
						<button className = 'jokelist-getmore'>Fetch Jokes</button>
					</div>
					
					<div className = 'jokelist-jokes'>
						<MyLoaders />	
					</div>
				</div>

				):(<div className = 'jokelist'>
					<div className = 'jokelist-sidebar' style = {blurStyle}>
						<h1 className = 'jokelist-title'><span>Dad</span> Jokes</h1>
						<img src = {emojirofl} alt = 'कुछ कर भाई'/>
						<button className = 'jokelist-getmore' onClick = {this.getJokes} >Fetch Jokes</button>
					</div>
					
					<div className = 'jokelist-jokes'>
						{this.state.jokes.map(jk => (
							<Joke
								thejoke = {jk.thejoke}
								votes = {jk.votes}
								key = {jk.id}
								upvote = {()=>this.handleVotes(jk.id, 1)}
								downvote = {()=>this.handleVotes(jk.id, -1)}
							/>
							))}
					</div>
				</div>
				)
				}
			</>
		)
	}
}

export default JokeList