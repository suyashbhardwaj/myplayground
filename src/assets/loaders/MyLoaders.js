import React from 'react'
import './MyLoaders01.css'

export class MyLoaders extends React.Component {
	render() {
		return (
			<div>
				<div className="box">
				  <div className="container">
				    <span className="circle"></span>
				    <span className="circle"></span>
				    <span className="circle"></span>
				    <span className="circle"></span>
				  </div>
				</div>
			</div>
		)
	}
}

export default MyLoaders